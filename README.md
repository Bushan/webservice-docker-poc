## Introduction

This is sample project to test the environment set up and also this README file serve as a living document which can be used to document all the findings and references used during POC implementation.

# Require setup

Below setup instructions are for Ubuntu 16.04. You need sudo permissions to do these installations

## Java 8

	$> sudo add-apt-repository ppa:webupd8team/java
	$> sudo apt update;sudo apt install oracle-java8-installer
	$> javac -version
	$> sudo apt install oracle-java8-set-default
	
## Maven

	$> sudo apt install maven

## Jenkins installation:
	
	https://wiki.jenkins-ci.org/display/JENKINS/Installing+Jenkins+on+Ubuntu

From above link below commands to be executed in order to run jenkins as service on port `8080` 

	$> wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
	$> sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
	$> sudo apt-get update
	$> sudo apt-get install jenkins
	
Build Item Goals and properties
	
	Goals : clean -X package docker:build -DpushImage
	
Properties: 

'docker.image.prefix' : Docker repo host is your nexus host and port what you provided for Docker HTTPS under Repository Connectors.
		
		docker.image.prefix= <Docker repo host:port> 
					e.g. ubuntuVB:18079
		
For 'registry.url' : Login as nexus admin and goto settings->Repositories-><Select URL from configured Docker repo>
		
		registry.url= <Secure docker register URL>
					e.g. https://192.168.1.98:8449/repository/myDockerRepo/
					
For 'docker.registry.id' use your docker repositiry name

	docker.registry.id=myDockerRepo

	
## Nexus Repo

	$> wget https://www.build-business-websites.co.uk/resources/nexus-repository-v3.tar.gz
	$> sudo cp ~/Downloads/nexus-3.2.0-01-unix.tar.gz /usr/local
	$> cd /usr/local
	$> ls
	$> sudo tar xvzf nexus-3.2.0-01-unix.tar.gz
	$> sudo ln -s nexus-3.2.0-01 nexus
	$> cd nexus-3.2.0-01/

 Setup Nexus home   
 
    Goto /etc/environment
    Add NEXUS_HOME="/usr/local/nexus"
    $> echo $NEXUS_HOME
 
 Start Nexus, run as service or run manually
  
    $> cd /usr/local/nexus
    $> ./bin/nexus start|run
    $> ./bin/nexus status
 
 Add nexus credentials in maven global settings xml `/usr/share/maven/conf/settings.xml`
 Provide valid username, password and email 
 		
 		<server>
 			<id>myDockerRepo</id>
 			<username>[username|admin></username>
 			<password>[password|admin123]</password>
 			<configuration>
 				<email>abc@email.com</email>
 			</configuration>
 		</server>>
	      		 	
 **Setting up Docker registry in Nexus requires secure connection. Use below link to generate self signed certificate
 	`http://www.eclipse.org/jetty/documentation/current/configuring-ssl.html``
 	
	$> keytool -keystore keystore -alias jetty -genkey -keyalg RSA -sigalg SHA256withRSA -validity 365
	
How to Enable the HTTPS Connector `https://books.sonatype.com/nexus-book/3.2/reference/security.html#ssl-inbound`
`https://books.sonatype.com/nexus-book/3.2/reference/security.html#enable-https``

Video Link:
	`https://www.youtube.com/watch?v=YzcvU802Az8``
	

	1. Create a Java keystore file at $install-dir/etc/ssl/keystore.jks which contains the Jetty SSL certificate to use. Instructions are available on the Eclipse Jetty documentation site.
	2. Edit $data-dir/etc/nexus.properties. Add a property on a new line application-port-ssl=8443. Change 8443 to be your preferred port on which to expose the HTTPS connector.
	3. Edit $data-dir/etc/nexus.properties. Change the nexus-args property comma delimited value to include ${jetty.etc}/jetty-https.xml. Save the file.
	4. Restart Nexus. Verify HTTPS connections can be established.
	5. Update the Base URL to use https in your repository manager configuration using the Base URL capability.
		
Verify that nexus runing on secure connection `HTTPS``

Follow URL `https://books.sonatype.com/nexus-book/3.2/reference/docker.html` 
    
## Docker

Follow instructions from `https://docs.docker.com/engine/installation/linux/ubuntu/``

Docker Pull images

	$ docker pull <repository tag>/<image name>:<image version>>
	 eg. docker pull ubuntuVB:18079/release:0.0.11
	 
Running under docker container

	$>docker run -p <avaible port>:<service port> -t <tag name>/<service-name>:<image version>
	e.g docker run -p 8761:8761 -t ubuntuVB:18079/discovery-service:0.0.1
	
Removing all untagged images

	docker rmi $(docker images | grep "<none>" | awk "{print $3}")
	
Removing all stopped containers

	docker rm $(docker ps -a -q)
	
# Living Doc:Reference and Findings

## Docker Image tagging
 To keep track of of images build with each build there are some recommeded option
 	ref: https://github.com/docker/docker/issues/13928   
 
 - Using Jenkins Build numbers
 As build numbers are unique they are good choice to tag images also it helps in tracking down commits for particular images.
 All our services are going to follow it
 
 - Using GIT commit Id
 This is another recommended approach as this can also uniquely identify image for every commit but good to use when we CI is for every commit. There is maven plugin 'git-cmmit-id-plugin' for getting commit id during build
  
 ref: https://github.com/ktoso/maven-git-commit-id-plugin/blob/master/README.md
	  https://www.alooma.com/blog/building-dockers

### Docker compose

 Should not be use to run service which are not dependent else future deployment will force to bring down those services too.  


### Linking dependent containers

For multiple containers dependencies use docker 'Links' command

For ex. Assume some_service depend on backened_service 

	$ docker run --name <custom_backend_name> -p <local port: expose port> -d <backend_service>:<version|latest>
	$ docker run  --name <custom_frontend_name> --link <custom_backend_name>:<backend_service> -p <local port>:<expose port> -d <front service>
	
 
### Use of Jenkins pipeline for CI

  //TODO : 'https://jenkins.io/2.0/'
 

### Useful References

	// TODO :

    
    


