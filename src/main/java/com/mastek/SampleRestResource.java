package com.mastek;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleRestResource {
	
	@RequestMapping("/")
    public String home() {
        return "Hello Docker World";
    }


}
